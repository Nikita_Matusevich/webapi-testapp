﻿import {Component} from "@angular/core";

@Component({
  selector: "app-root",
  template: `
    <application-window>
      <router-outlet></router-outlet>
    </application-window>
  `,
  styles: []
})
export class AppComponent {}
