﻿import {Component, OnInit} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {MatSnackBar} from "@angular/material/snack-bar";

import {LabelDialogComponent} from "../../../entryComponents/label.dialog";
import {NotificationSnackBarComponent} from "../../../entryComponents/notification.snackbar";
import {ArrayHelper} from "../../../helpers/array.helper";
import {LabelViewModel} from "../../../models/label.model";
import {LabelsService} from "../../../services/labels.service";

@Component({
  templateUrl: "./home.labels.component.html",
  styleUrls: [`./home.labels.component.css`]
})
export class LabelsComponent implements OnInit {
  public models: LabelViewModel[] = [];
  public constructor(
    private labelsService: LabelsService,

    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {}

  public ngOnInit(): void {
    this.labelsService.getAll(response => {
      if (response) {
        this.models = response;
      }
    });
  }

  public onCreate(model: LabelViewModel = null): void {
    const dialogRef = this.dialog.open(LabelDialogComponent, {
      width: "500px",
      data: {
        model: model ? model : null
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (result.id) {
          this.labelsService.update(result, response => {
            if (response) {
              this.openNotificationSnackBar("Saved successfully", true);
              this.models = this.models.map(x => {
                if (x.id === response.id) {
                  return response;
                } else {
                  return x;
                }
              });
            } else {
              this.openNotificationSnackBar("An error occurred", false);
            }
          });
        } else {
          this.labelsService.create(result, response => {
            if (response) {
              this.openNotificationSnackBar("Created successfully", true);
              this.models.push(response);
            } else {
              this.openNotificationSnackBar("An error occurred", false);
            }
          });
        }
      }
    });
  }

  public onDelete(model: LabelViewModel): void {
    this.labelsService.delete(model.id, response => {
      if (response) {
        this.openNotificationSnackBar("Deleted successfully", true);
        this.models = this.models.filter(x => x.id !== response);
      } else {
        this.openNotificationSnackBar("An error occurred", false);
      }
    });
  }

  public getRows(models: LabelViewModel[], count: number): any[] {
    const sorted = models.sort((x, y) => {
      if (x.id > y.id) {
        return 1;
      } else if (x.id < y.id) {
        return -1;
      } else {
        return 0;
      }
    });
    return ArrayHelper.split(sorted, count);
  }

  public openNotificationSnackBar(message: string, success: boolean) {
    this.snackBar.openFromComponent(NotificationSnackBarComponent, {
      duration: 3000,
      data: {
        message: message,
        success: success
      }
    });
  }
}
