﻿import {Component} from "@angular/core";
import {Router} from "@angular/router";

@Component({
  selector: "app-home-root",
  templateUrl: "./home.component.html",
  styleUrls: [`./home.component.css`]
})
export class HomeComponent {
  public constructor(private router: Router) {}

  public menuOpened: boolean = false;

  public scrollToTop(duration: number) {
    const scrollStep = -window.scrollY / (duration / 15);
    const scrollInterval = setInterval(() => {
      if (window.scrollY !== 0) {
        window.scrollBy(0, scrollStep);
      } else {
        clearInterval(scrollInterval);
      }
    }, 15);
  }

  public isActive(url: string): boolean {
    return this.router.isActive(url, true);
  }
}
