﻿import {Component, OnInit} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {MatSnackBar} from "@angular/material/snack-bar";

import {AttachUserDialogComponent} from "../../../entryComponents/attachUser.dialog";
import {NotificationSnackBarComponent} from "../../../entryComponents/notification.snackbar";
import {TaskDialogComponent} from "../../../entryComponents/task.dialog";
import {ArrayHelper} from "../../../helpers/array.helper";
import {Statuses, StatusViewModel} from "../../../models/status.model";
import {TaskViewModel} from "../../../models/task.model";
import {TasksService} from "../../../services/tasks.service";

@Component({
  templateUrl: "./home.tasks.component.html",
  styleUrls: [`./home.tasks.component.css`]
})
export class TasksComponent implements OnInit {
  public models: TaskViewModel[] = [];
  public statuses: StatusViewModel[] = [];
  public activeStatus: string = "";

  public constructor(
    private tasksService: TasksService,

    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {}

  public ngOnInit(): void {
    this.tasksService.getAll(response => {
      if (response) {
        this.models = response;
      }
    });
    this.statuses = Statuses;
    this.activeStatus = this.statuses[0].value;
  }

  public onCreate(model: TaskViewModel = null): void {
    const dialogRef = this.dialog.open(TaskDialogComponent, {
      width: "500px",
      data: {
        model: model ? model : null
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (result.id) {
          const responseCallback = (response: TaskViewModel) => {
            if (response) {
              this.openNotificationSnackBar("Saved successfully", true);
              this.models = this.models.map(x => {
                if (x.id === response.id) {
                  return response;
                } else {
                  return x;
                }
              });
            } else {
              this.openNotificationSnackBar("An error occurred", false);
            }
          };

          const updatedFields = Object.keys(model)
            .filter(key => typeof model[key] !== "object")
            .filter(x => model[x] !== result[x]);

          if (updatedFields.length === 0) return;
          else if (updatedFields.length === 1)
            this.tasksService.updateField(result.id, updatedFields[0], result[updatedFields[0]], responseCallback);
          else this.tasksService.update(result, responseCallback);
        } else {
          this.tasksService.create(result, response => {
            if (response) {
              this.openNotificationSnackBar("Created successfully", true);
              this.models.push(response);
            } else {
              this.openNotificationSnackBar("An error occurred", false);
            }
          });
        }
      }
    });
  }

  public onDelete(model: TaskViewModel): void {
    this.tasksService.delete(model.id, response => {
      if (response) {
        this.openNotificationSnackBar("Deleted successfully", true);
        this.models = this.models.filter(x => x.id !== response);
      } else {
        this.openNotificationSnackBar("An error occurred", false);
      }
    });
  }

  public onAttach(model: TaskViewModel): void {
    const dialogRef = this.dialog.open(AttachUserDialogComponent, {
      width: "500px",
      data: {
        model: model ? model : null
      }
    });

    dialogRef.afterClosed().subscribe((result: TaskViewModel) => {
      if (result) {
        this.models = this.models.map(x => {
          if (x.id === result.id) {
            return result;
          } else {
            return x;
          }
        });
      }
    });
  }

  public getRows(taskStatus: string, tasks: TaskViewModel[], count: number): any[] {
    const sorted = tasks
      .filter(x => x.status === taskStatus)
      .sort((x, y) => {
        if (x.id > y.id) {
          return 1;
        } else if (x.id < y.id) {
          return -1;
        } else {
          return 0;
        }
      });
    return ArrayHelper.split(sorted, count);
  }

  public getStatusTitle(statusValue: string): string {
    const status = this.statuses.filter(x => x.value === statusValue)[0];
    if (!status) {
      return "";
    }
    return status.title;
  }

  public getStatusCount(statusValue: string): number {
    return this.models.filter(x => x.status === statusValue).length;
  }

  public openNotificationSnackBar(message: string, success: boolean) {
    this.snackBar.openFromComponent(NotificationSnackBarComponent, {
      duration: 3000,
      data: {
        message: message,
        success: success
      }
    });
  }
}
