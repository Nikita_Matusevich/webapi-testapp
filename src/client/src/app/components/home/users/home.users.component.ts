﻿import {Component, OnInit} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {MatSnackBar} from "@angular/material/snack-bar";

import {NotificationSnackBarComponent} from "../../../entryComponents/notification.snackbar";
import {UserDialogComponent} from "../../../entryComponents/user.dialog";
import {UserViewModel} from "../../../models/user.model";
import {UsersService} from "../../../services/users.service";

@Component({
  templateUrl: "./home.users.component.html",
  styleUrls: [`./home.users.component.css`]
})
export class UsersComponent implements OnInit {
  public models: UserViewModel[] = [];
  public constructor(
    private usersService: UsersService,

    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {}

  public ngOnInit(): void {
    this.usersService.getAll(response => {
      if (response) {
        this.models = response;
      }
    });
  }

  public onCreate(model: UserViewModel = null): void {
    const dialogRef = this.dialog.open(UserDialogComponent, {
      width: "500px",
      data: {
        model: model ? model : null
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (result.id) {
          this.usersService.update(result, response => {
            if (response) {
              this.openNotificationSnackBar("Saved successfully", true);
              this.models = this.models.map(x => {
                if (x.id === response.id) {
                  return response;
                } else {
                  return x;
                }
              });
            } else {
              this.openNotificationSnackBar("An error occurred", false);
            }
          });
        } else {
          this.usersService.create(result, response => {
            if (response) {
              this.openNotificationSnackBar("Created successfully", true);
              this.models.push(response);
            } else {
              this.openNotificationSnackBar("An error occurred", false);
            }
          });
        }
      }
    });
  }

  public onDelete(model: UserViewModel): void {
    this.usersService.delete(model.id, response => {
      if (response) {
        this.openNotificationSnackBar("Deleted successfully", true);
        this.models = this.models.filter(x => x.id !== response);
      } else {
        this.openNotificationSnackBar("An error occurred", false);
      }
    });
  }

  public getItems(models: UserViewModel[]): any[] {
    return models.sort((x, y) => {
      if (x.id > y.id) {
        return 1;
      } else if (x.id < y.id) {
        return -1;
      } else {
        return 0;
      }
    });
  }

  public openNotificationSnackBar(message: string, success: boolean) {
    this.snackBar.openFromComponent(NotificationSnackBarComponent, {
      duration: 3000,
      data: {
        message: message,
        success: success
      }
    });
  }
}
