﻿import {Component, Inject} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

import {TaskViewModel} from "../models/task.model";
import {UserViewModel} from "../models/user.model";
import {TasksService} from "../services/tasks.service";
import {UsersService} from "../services/users.service";

@Component({
  selector: "app-attach-user-dialog",
  template: `<div>
    <h2 mat-dialog-title>
      {{ title }}
      <button type="button" class="close" [aria-label]="'Close'" (click)="onSubmit()">
        <span aria-hidden="true">
          <mat-icon svgIcon="window-close"></mat-icon>
        </span>
      </button>
    </h2>
    <mat-dialog-content>
      <table class="table">
        <thead class="thead-dark">
          <tr>
            <th scope="col">#</th>
            <th scope="col">First</th>
            <th scope="col">Last</th>
            <th scope="col">Position</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          <tr *ngFor="let user of users; let i = index">
            <th scope="row">{{ i + 1 }}</th>
            <td>{{ user.firstName }}</td>
            <td>{{ user.lastName }}</td>
            <td>{{ user.position }}</td>
            <td>
              <button (click)="onAction(user)" class="btn btn-primary">
                <i [class]="actionClass(user)"></i>
              </button>
            </td>
          </tr>
        </tbody>
      </table>
    </mat-dialog-content>
    <mat-dialog-actions>
      <button mat-button (click)="onSubmit()">{{ button }}</button>
    </mat-dialog-actions>
  </div> `,
  styles: [``]
})
export class AttachUserDialogComponent {
  public model: TaskViewModel = null;
  public users: UserViewModel[] = [];

  public title: string;
  public button: string;

  public constructor(
    public usersService: UsersService,
    public tasksService: TasksService,
    public dialogRef: MatDialogRef<AttachUserDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.model = data.model ? data.model : new UserViewModel();

    this.usersService.getAll(response => {
      this.users = response ? response : [];
    });

    this.title = "Attach Users";
    this.button = "Close";
  }

  public actionClass(user: UserViewModel): string {
    const exists = this.model.users.filter(x => x.id === user.id)[0];
    return `fas fa-${exists ? "minus" : "plus"}`;
  }

  public onAction(user: UserViewModel): void {
    const exists = this.model.users.filter(x => x.id === user.id)[0];
    if (exists) {
      this.tasksService.deleteUser(this.model, user, response => {
        if (response) {
          this.model.users = this.model.users.filter(x => x.id !== user.id);
        }
      });
    } else {
      this.tasksService.addUser(this.model, user, response => {
        if (response) {
          this.model.users.push(user);
        }
      });
    }
  }

  public onSubmit(): void {
    this.dialogRef.close(this.model);
  }
}
