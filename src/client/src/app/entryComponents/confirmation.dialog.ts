﻿import {Component, Inject} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: "app-confirmation-mat-dialog",
  template: `<div>
    <h2 mat-dialog-title>
      {{ dialogTitle }}
    </h2>
    <mat-dialog-content>
      <h4>{{ message }}</h4>
    </mat-dialog-content>
    <mat-dialog-actions>
      <button mat-button [mat-dialog-close]="true">{{ okButton }}</button>
      <button mat-button [mat-dialog-close]="false">{{ cancelButton }}</button>
    </mat-dialog-actions>
  </div> `,
  styles: [``]
})
export class ConfirmationDialogComponent {
  public message: string = "";
  public okButton: string = "";
  public cancelButton: string = "";
  public dialogTitle: string = "";

  public constructor(
    public dialogRef: MatDialogRef<ConfirmationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.dialogRef.disableClose = true;
    this.dialogTitle = data && data.dialogTitle ? data.dialogTitle : "Confirmation";
    this.message = data && data.message ? data.message : "";
    this.okButton = data && data.okButton ? data.okButton : "Ok";
    this.cancelButton = data && data.cancelButton ? data.cancelButton : "Cancel";
  }
}
