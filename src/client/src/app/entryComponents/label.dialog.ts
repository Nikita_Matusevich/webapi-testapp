﻿import {Component, Inject} from "@angular/core";
import {FormControl, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

import {LabelViewModel} from "../models/label.model";

@Component({
  selector: "app-label-dialog",
  template: `<div>
    <h2 mat-dialog-title>
      {{ title }}
      <button type="button" class="close" [aria-label]="'Close'" [mat-dialog-close]="false">
        <span aria-hidden="true">
          <mat-icon svgIcon="window-close"></mat-icon>
        </span>
      </button>
    </h2>
    <mat-dialog-content>
      <div class="form-group">
        <label class="col-md-4">Title</label>
        <mat-form-field class="col-md-8">
          <input matInput placeholder="Title" [formControl]="titleFormControl" />
          <mat-hint align="end">{{ titleFormControl.value ? titleFormControl.value.length : 0 }} / 100</mat-hint>
          <mat-error *ngIf="titleFormControl.hasError('required')"> Title is <strong>required</strong> </mat-error>
        </mat-form-field>
      </div>

      <div class="form-group">
        <label class="col-md-4">Color</label>
        <mat-form-field class="col-md-8">
          <input matInput type="color" placeholder="Color" [formControl]="colorFormControl" />
          <mat-error *ngIf="colorFormControl.hasError('required')"> Estimated Time is <strong>required</strong> </mat-error>
        </mat-form-field>
      </div>
    </mat-dialog-content>
    <mat-dialog-actions>
      <button mat-button (click)="onSubmit()">{{ button }}</button>
    </mat-dialog-actions>
  </div> `,
  styles: [``]
})
export class LabelDialogComponent {
  public titleFormControl: FormControl;
  public colorFormControl: FormControl;

  public model: LabelViewModel = null;
  public title: string;
  public button: string;

  public constructor(
    public dialogRef: MatDialogRef<LabelDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.model = data.model ? data.model : new LabelViewModel();

    this.titleFormControl = new FormControl(this.model.title ? this.model.title : "", [Validators.required]);

    this.colorFormControl = new FormControl(this.model.hexColor ? this.model.hexColor : "", [Validators.required]);

    this.title = this.model.id ? "Edit" : "New";
    this.button = this.model.id ? "Save" : "Submit";
  }

  public onSubmit(): void {
    if (this.titleFormControl.invalid || this.colorFormControl.invalid) {
      return;
    }

    this.model.title = this.titleFormControl.value;
    this.model.hexColor = this.colorFormControl.value;
    this.dialogRef.close(this.model);
  }
}
