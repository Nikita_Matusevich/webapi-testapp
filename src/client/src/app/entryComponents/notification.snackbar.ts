﻿import {Component, Inject} from "@angular/core";
import {MAT_SNACK_BAR_DATA, MatSnackBarRef} from "@angular/material/snack-bar";

@Component({
  selector: "app-notification-snack-bar",
  template: `{{ data.message }}
    <button mat-button class="mat-agency-notification-snackbar-action" (click)="onClosed()" [color]="data.success ? 'primary' : 'warn'">
      Close
    </button>`,
  styles: [
    `
      .mat-agency-notification-snackbar {
        display: flex;
        justify-content: space-between;
        line-height: 20px;
        opacity: 1;
      }
      .mat-agency-notification-snackbar-action {
        background: none;
        flex-shrink: 0;
        position: absolute;
        right: 15px;
        top: 7px;
      }
    `
  ],
  // eslint-disable-next-line @angular-eslint/no-host-metadata-property
  host: {
    class: "mat-agency-notification-snackbar"
  }
})
export class NotificationSnackBarComponent {
  public constructor(
    public snackBarRef: MatSnackBarRef<NotificationSnackBarComponent>,
    @Inject(MAT_SNACK_BAR_DATA) public data: any
  ) {}

  public onClosed() {
    this.snackBarRef.dismiss();
  }
}
