﻿import {Component, Inject} from "@angular/core";
import {FormControl, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

import {LabelViewModel} from "../models/label.model";
import {Statuses, StatusViewModel} from "../models/status.model";
import {TaskViewModel} from "../models/task.model";
import {LabelsService} from "../services/labels.service";
import {TasksService} from "../services/tasks.service";

@Component({
  selector: "app-task-dialog",
  template: `<div>
    <h2 mat-dialog-title>
      {{ title }}
      <button type="button" class="close" [aria-label]="'Close'" [mat-dialog-close]="false">
        <span aria-hidden="true">
          <mat-icon svgIcon="window-close"></mat-icon>
        </span>
      </button>
    </h2>
    <mat-dialog-content>
      <div class="form-group">
        <label class="col-md-4">Title</label>
        <mat-form-field class="col-md-8">
          <input matInput placeholder="Title" [formControl]="titleFormControl" />
          <mat-hint align="end">{{ titleFormControl.value ? titleFormControl.value.length : 0 }} / 256</mat-hint>
          <mat-error *ngIf="titleFormControl.hasError('required')"> Title is <strong>required</strong> </mat-error>
        </mat-form-field>
      </div>

      <div class="form-group">
        <label class="col-md-4">Description</label>
        <mat-form-field class="col-md-8">
          <textarea matInput placeholder="Description" [formControl]="descriptionFormControl"></textarea>
          <mat-hint align="end">{{ descriptionFormControl.value ? descriptionFormControl.value.length : 0 }} / 2000</mat-hint>
          <mat-error *ngIf="descriptionFormControl.hasError('required')"> Description is <strong>required</strong> </mat-error>
        </mat-form-field>
      </div>

      <div class="form-group">
        <label class="col-md-4">Status</label>
        <mat-form-field class="col-md-8">
          <mat-select placeholder="Status" [formControl]="statusFormControl">
            <mat-option>--</mat-option>
            <mat-option *ngFor="let status of statuses" [value]="status.value">
              {{ status.title }}
            </mat-option>
          </mat-select>
          <mat-error *ngIf="statusFormControl.hasError('required')"> Status is <strong>required</strong> </mat-error>
        </mat-form-field>
      </div>

      <div class="form-group">
        <label class="col-md-4">Estimated Time</label>
        <mat-form-field class="col-md-8">
          <input matInput type="datetime-local" placeholder="Estimated Time mm-dd-yyyy" [formControl]="estimatedTime" />
          <mat-error *ngIf="estimatedTime.hasError('required')"> Estimated Time is <strong>required</strong> </mat-error>
        </mat-form-field>
      </div>

      <div *ngIf="model.id">
        <h6>Labels</h6>
        <span
          *ngFor="let label of labels"
          class="badge badge-pill mr-1"
          [style.background-color]="label.hexColor"
          [style.cursor]="'pointer'"
          [style.opacity]="labelOpacity(label)"
          (click)="onToggle(label)"
        >
          {{ label.title }}
        </span>
      </div>
    </mat-dialog-content>
    <mat-dialog-actions>
      <button mat-button (click)="onSubmit()">{{ button }}</button>
    </mat-dialog-actions>
  </div> `,
  styles: [``]
})
export class TaskDialogComponent {
  public titleFormControl: FormControl;
  public descriptionFormControl: FormControl;
  public statusFormControl: FormControl;
  public estimatedTime: FormControl;

  public model: TaskViewModel = null;
  public title: string;
  public button: string;

  public statuses: StatusViewModel[];
  public labels: LabelViewModel[] = [];

  public constructor(
    private tasksService: TasksService,
    private labelsService: LabelsService,
    public dialogRef: MatDialogRef<TaskDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.model = data.model ? Object.assign({}, data.model) : new TaskViewModel();

    this.titleFormControl = new FormControl(this.model.title ? this.model.title : "", [Validators.required]);

    this.descriptionFormControl = new FormControl(this.model.description ? this.model.description : "", []);

    this.statusFormControl = new FormControl(this.model.status ? this.model.status : "", [Validators.required]);

    this.estimatedTime = new FormControl(this.model.estimatedTime ? this.model.estimatedTime : "", []);

    this.statuses = Statuses;

    this.labelsService.getAll(response => {
      this.labels = response ? response : [];
    });

    this.title = this.model.id ? "Edit" : "New";
    this.button = this.model.id ? "Save" : "Submit";
  }

  public onSubmit(): void {
    if (
      this.titleFormControl.invalid ||
      this.descriptionFormControl.invalid ||
      this.statusFormControl.invalid ||
      this.estimatedTime.invalid
    ) {
      return;
    }

    this.model.title = this.titleFormControl.value;
    this.model.description = this.descriptionFormControl.value;
    this.model.status = this.statusFormControl.value;
    this.model.estimatedTime = this.estimatedTime.value;
    this.dialogRef.close(this.model);
  }

  public labelOpacity(label: LabelViewModel): string {
    const exists = label && this.model.labels.filter(x => x.id === label.id)[0];
    return exists ? "1.0" : "0.3";
  }

  public onToggle(label: LabelViewModel): void {
    const exists = label && this.model.labels.filter(x => x.id === label.id)[0];
    if (exists) {
      this.tasksService.deleteLabel(this.model, label, response => {
        if (response) {
          this.model.labels = this.model.labels.filter(x => x.id !== label.id);
        }
      });
    } else {
      this.tasksService.addLabel(this.model, label, response => {
        if (response) {
          this.model.labels.push(label);
        }
      });
    }
  }
}
