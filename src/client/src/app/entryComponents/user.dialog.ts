﻿import {Component, Inject} from "@angular/core";
import {FormControl, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

import {UserViewModel} from "../models/user.model";

@Component({
  selector: "app-user-dialog",
  template: `<div>
    <h2 mat-dialog-title>
      {{ title }}
      <button type="button" class="close" [aria-label]="'Close'" [mat-dialog-close]="false">
        <span aria-hidden="true">
          <mat-icon svgIcon="window-close"></mat-icon>
        </span>
      </button>
    </h2>
    <mat-dialog-content>
      <div class="form-group">
        <label class="col-md-4">First Name</label>
        <mat-form-field class="col-md-8">
          <input matInput placeholder="First Name" [formControl]="firstFormControl" />
          <mat-hint align="end">{{ firstFormControl.value ? firstFormControl.value.length : 0 }} / 100</mat-hint>
          <mat-error *ngIf="firstFormControl.hasError('required')"> First Name is <strong>required</strong> </mat-error>
        </mat-form-field>
      </div>

      <div class="form-group">
        <label class="col-md-4">Last Name</label>
        <mat-form-field class="col-md-8">
          <input matInput placeholder="Last Name" [formControl]="lastFormControl" />
          <mat-hint align="end">{{ lastFormControl.value ? lastFormControl.value.length : 0 }} / 100</mat-hint>
          <mat-error *ngIf="lastFormControl.hasError('required')"> Last Name is <strong>required</strong> </mat-error>
        </mat-form-field>
      </div>

      <div class="form-group">
        <label class="col-md-4">Email</label>
        <mat-form-field class="col-md-8">
          <input matInput type="email" placeholder="Email" [formControl]="emailFormControl" />
          <mat-hint align="end">{{ lastFormControl.value ? emailFormControl.value.length : 0 }} / 100</mat-hint>
          <mat-error *ngIf="emailFormControl.hasError('required')"> Email is <strong>required</strong> </mat-error>
        </mat-form-field>
      </div>

      <div class="form-group">
        <label class="col-md-4">Position</label>
        <mat-form-field class="col-md-8">
          <input matInput placeholder="Position" [formControl]="positionFormControl" />
          <mat-hint align="end">{{ positionFormControl.value ? positionFormControl.value.length : 0 }} / 100</mat-hint>
          <mat-error *ngIf="positionFormControl.hasError('required')"> Position is <strong>required</strong> </mat-error>
        </mat-form-field>
      </div>
    </mat-dialog-content>
    <mat-dialog-actions>
      <button mat-button (click)="onSubmit()">{{ button }}</button>
    </mat-dialog-actions>
  </div> `,
  styles: [``]
})
export class UserDialogComponent {
  public firstFormControl: FormControl;
  public lastFormControl: FormControl;
  public emailFormControl: FormControl;
  public positionFormControl: FormControl;

  public model: UserViewModel = null;
  public title: string;
  public button: string;

  public constructor(
    public dialogRef: MatDialogRef<UserDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.model = data.model ? data.model : new UserViewModel();

    this.firstFormControl = new FormControl(this.model.firstName ? this.model.firstName : "", [Validators.required]);

    this.lastFormControl = new FormControl(this.model.lastName ? this.model.lastName : "", [Validators.required]);

    this.emailFormControl = new FormControl(this.model.email ? this.model.email : "", [Validators.required]);

    this.positionFormControl = new FormControl(this.model.position ? this.model.position : "", [
      //Validators.required
    ]);

    this.title = this.model.id ? "Edit" : "New";
    this.button = this.model.id ? "Save" : "Submit";
  }

  public onSubmit(): void {
    if (
      this.firstFormControl.invalid ||
      this.lastFormControl.invalid ||
      this.emailFormControl.invalid ||
      this.positionFormControl.invalid
    ) {
      return;
    }

    this.model.firstName = this.firstFormControl.value;
    this.model.lastName = this.lastFormControl.value;
    this.model.email = this.emailFormControl.value;
    this.model.position = this.positionFormControl.value;
    this.dialogRef.close(this.model);
  }
}
