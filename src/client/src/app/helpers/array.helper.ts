﻿export const ArrayHelper = {
  split(array: any[], length: number): any[] {
    if (!array) {
      return [];
    }
    const clone = array.slice();
    const result: any[] = [];
    while (clone.length) {
      result.push(clone.splice(0, length));
    }
    return result;
  },

  splitWithOffset(array: any[], length: number, offset: number): any[] {
    if (!array) {
      return [];
    }
    const clone = array.slice();
    const result: any[] = [];
    result.push(clone.splice(0, length - offset));
    while (clone.length) {
      result.push(clone.splice(0, length));
    }
    return result;
  },

  orderBy<T>(array: T[], field: string = null): T[] {
    if (!array || array.length < 2) {
      return array;
    }
    if (!field || array[0][field] === null || array[0][field] === undefined) {
      return array.sort();
    } else {
      return array.sort((a, b) => (a[field] > b[field] ? 1 : b[field] > a[field] ? -1 : 0));
    }
  },

  orderByDescending<T>(array: T[], field: string = null): T[] {
    if (!array || array.length < 2) {
      return array;
    }
    if (!field || array[0][field] === null || array[0][field] === undefined) {
      return array.sort();
    } else {
      return array.sort((a, b) => (a[field] > b[field] ? -1 : b[field] > a[field] ? 1 : 0));
    }
  }
};
