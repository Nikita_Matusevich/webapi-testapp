﻿export const DateHelper = {
  full(date: any): string {
    let dateValue: Date = null;
    if (date instanceof Date) {
      dateValue = date;
    } else if (typeof date === "string") {
      dateValue = new Date(date);
    }

    const day = dateValue.getDate();
    const month = dateValue.getMonth() + 1;
    const year = dateValue.getFullYear();
    const hours = dateValue.getHours();
    const minutes = dateValue.getMinutes();

    return `${month}/${day}/${year} ${hours}:${minutes}`;
  },

  fullWithZeros(date: any): string {
    const format = (num: number) => {
      if (num > 0 && num < 10) {
        return `0${num}`;
      } else {
        return num;
      }
    };

    let dateValue: Date = null;
    if (date instanceof Date) {
      dateValue = date;
    } else if (typeof date === "string") {
      dateValue = new Date(date);
    }

    const day = format(dateValue.getDate());
    const month = format(dateValue.getMonth() + 1);
    const year = format(dateValue.getFullYear());
    const hours = format(dateValue.getHours());
    const minutes = format(dateValue.getMinutes());

    return `${month}/${day}/${year} ${hours}:${minutes}`;
  },
  getWithoutTimezone(date: any) {
    if (date instanceof Date) {
      const anyDate: any = date;
      const tzoffset = new Date().getTimezoneOffset() * 60000;
      return new Date(anyDate - tzoffset).toISOString().slice(0, -1);
    } else if (typeof date === "string") {
      return date;
    } else {
      return null;
    }
  }
};
