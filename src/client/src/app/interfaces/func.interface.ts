﻿export interface Func<T, TResult> {
  (arg: T): TResult;
}

export interface Func2<T1, T2, TResult> {
  (arg1: T1, arg2: T2): TResult;
}

export interface Func3<T1, T2, T3, TResult> {
  (arg1: T1, arg2: T2, arg3: T3): TResult;
}

export interface Func4<T1, T2, T3, T4, TResult> {
  (arg1: T1, arg2: T2, arg3: T3, arg4: T4): TResult;
}

export interface Func5<T1, T2, T3, T4, T5, TResult> {
  (arg1: T1, arg2: T2, arg3: T3, arg4: T4, arg5: T5): TResult;
}
