﻿import {TaskViewModel} from "./task.model";

export class LabelViewModel {
  public constructor() {
    this.tasks = [];
  }

  public id: number;
  public title: string;
  public hexColor: string;
  public tasks: TaskViewModel[];
}
