﻿export class StatusViewModel {
  public title: string;
  public value: string;
}

export const Statuses: StatusViewModel[] = [
  {
    title: "Created",
    value: "created"
  },
  {
    title: "In process",
    value: "started"
  },
  {
    title: "Completed",
    value: "finished"
  }
];
