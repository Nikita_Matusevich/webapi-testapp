﻿import {LabelViewModel} from "./label.model";
import {UserViewModel} from "./user.model";

export class TaskViewModel {
  public constructor() {
    this.users = [];
    this.labels = [];
  }

  public id: number;
  public title: string;
  public description: string;
  public dateCreated: Date;
  public estimatedTime: Date;
  public status: string;
  public labels: LabelViewModel[];
  public users: UserViewModel[];
}
