﻿export class UserViewModel {
  public id: number;
  public firstName: string;
  public lastName: string;
  public position: string;
  public email: string;
}
