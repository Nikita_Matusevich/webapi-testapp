﻿import {DatePipe} from "@angular/common";
import {HttpClientModule} from "@angular/common/http";
import {NgModule, NO_ERRORS_SCHEMA} from "@angular/core";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatIconModule, MatIconRegistry} from "@angular/material/icon";
import {BrowserModule} from "@angular/platform-browser";
import {DomSanitizer} from "@angular/platform-browser";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {LoadingBarHttpClientModule} from "@ngx-loading-bar/http-client";
import {LoadingBarRouterModule} from "@ngx-loading-bar/router";

import {PartySamplerMaterialModule} from "./material.module";

import {AppComponent} from "../components/app.component";
import {LabelsComponent} from "../components/home/labels/home.labels.component";
import {HomeComponent} from "../components/home/layout/home.component";
import {TasksComponent} from "../components/home/tasks/home.tasks.component";
import {UsersComponent} from "../components/home/users/home.users.component";
import {AttachUserDialogComponent} from "../entryComponents/attachUser.dialog";
import {ConfirmationDialogComponent} from "../entryComponents/confirmation.dialog";
import {LabelDialogComponent} from "../entryComponents/label.dialog";
import {NotificationSnackBarComponent} from "../entryComponents/notification.snackbar";
import {TaskDialogComponent} from "../entryComponents/task.dialog";
import {UserDialogComponent} from "../entryComponents/user.dialog";
import {AppRouting} from "../routings/app.routing";
import {LabelsService} from "../services/labels.service";
import {RequestsService} from "../services/requests.service";
import {TasksService} from "../services/tasks.service";
import {UsersService} from "../services/users.service";

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    PartySamplerMaterialModule,
    MatIconModule,
    LoadingBarHttpClientModule,
    LoadingBarRouterModule,
    AppRouting
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    TasksComponent,
    LabelsComponent,
    UsersComponent,
    NotificationSnackBarComponent,
    ConfirmationDialogComponent,
    TaskDialogComponent,
    LabelDialogComponent,
    UserDialogComponent,
    AttachUserDialogComponent
  ],
  bootstrap: [AppComponent],
  providers: [RequestsService, TasksService, LabelsService, UsersService, DatePipe, {provide: "Window", useValue: window}],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule {
  public constructor(mdIconRegistry: MatIconRegistry, domSanitizer: DomSanitizer) {
    mdIconRegistry.addSvgIconSet(domSanitizer.bypassSecurityTrustResourceUrl("styles/material-design-icons.svg"));
  }
}
