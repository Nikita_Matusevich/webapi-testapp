﻿import {PreloadAllModules, RouterModule, Routes} from "@angular/router";

import {LabelsComponent} from "../components/home/labels/home.labels.component";
import {HomeComponent} from "../components/home/layout/home.component";
import {TasksComponent} from "../components/home/tasks/home.tasks.component";
import {UsersComponent} from "../components/home/users/home.users.component";

const appRoutes: Routes = [
  {
    path: "",
    component: HomeComponent,
    children: [
      {path: "", component: TasksComponent},
      {path: "labels", component: LabelsComponent},
      {path: "users", component: UsersComponent}
    ]
  },
  {
    path: "**",
    redirectTo: ""
  }
];

export const AppRouting = RouterModule.forRoot(appRoutes, {
  useHash: false,
  preloadingStrategy: PreloadAllModules
});
