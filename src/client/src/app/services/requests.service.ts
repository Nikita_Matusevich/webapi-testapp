﻿import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";

import {environment} from "src/environments/environment";

@Injectable()
export class RequestsService {
  private _headers: HttpHeaders;

  public constructor(private httpClient: HttpClient) {
    this._headers = new HttpHeaders().set("Content-Type", "application/json;charset=utf-8");
  }

  public getData<T>(url: string, headers: any = false): Observable<HttpResponse<T>> {
    return this.httpClient.get<T>(this.getUrl(url), {
      headers: headers ? headers : this._headers,
      observe: "response"
    });
  }

  public postData<T>(url: string, body: any, headers: any = false): Observable<HttpResponse<T>> {
    return this.httpClient.post<T>(this.getUrl(url), body, {
      headers: headers ? headers : this._headers,
      observe: "response"
    });
  }

  public deleteData<T>(url: string, headers: any = false): Observable<HttpResponse<T>> {
    return this.httpClient.delete<T>(this.getUrl(url), {
      headers: headers ? headers : this._headers,
      observe: "response"
    });
  }

  public putData<T>(url: string, body: any, headers: any = false): Observable<HttpResponse<T>> {
    return this.httpClient.put<T>(this.getUrl(url), body, {
      headers: headers ? headers : this._headers,
      observe: "response"
    });
  }

  public patchData<T>(url: string, body: any, headers: any = false): Observable<HttpResponse<T>> {
    return this.httpClient.patch<T>(this.getUrl(url), body, {
      headers: headers ? headers : this._headers,
      observe: "response"
    });
  }

  public getUrl(url: string) {
    return [`${location.protocol}//${environment.baseUrl}`, "api", url].join("/");
  }
}
