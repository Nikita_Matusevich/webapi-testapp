﻿import {HttpErrorResponse} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Router} from "@angular/router";
import {catchError, throwError} from "rxjs";

import {Action} from "../interfaces/action.interface";
import {LabelViewModel} from "../models/label.model";
import {TaskViewModel} from "../models/task.model";
import {UserViewModel} from "../models/user.model";

import {RequestsService} from "./requests.service";

@Injectable()
export class TasksService {
  private baseUrl: string = "tasks";

  public constructor(
    private requestsService: RequestsService,
    private router: Router
  ) {}

  public getAll(callback: Action<TaskViewModel[]>) {
    this.requestsService
      .getData<TaskViewModel[]>(this.baseUrl)
      .pipe(
        catchError(err => {
          this.onError(err, callback);
          return throwError(() => err);
        })
      )
      .subscribe(response => {
        if (response.ok && callback) {
          callback(response.body);
        } else {
          callback(null);
        }
      });
  }

  public getById(id: number, callback: Action<TaskViewModel>) {
    if (!id) {
      return;
    }
    this.requestsService
      .getData<TaskViewModel>([this.baseUrl, id].join("/"))
      .pipe(
        catchError(err => {
          this.onError(err, callback);
          return throwError(() => err);
        })
      )
      .subscribe(response => {
        if (response.ok && callback) {
          callback(response.body);
        } else {
          callback(null);
        }
      });
  }

  public create(data: TaskViewModel, callback: Action<TaskViewModel>) {
    if (!data) {
      if (callback) {
        callback(null);
      }
      return;
    }

    this.requestsService
      .postData<TaskViewModel>(this.baseUrl, data)
      .pipe(
        catchError(err => {
          this.onError(err, callback);
          return throwError(() => err);
        })
      )
      .subscribe(response => {
        if (response.ok && callback) {
          callback(response.body);
        } else {
          callback(null);
        }
      });
  }

  public update(data: TaskViewModel, callback: Action<TaskViewModel>) {
    if (!data) {
      if (callback) {
        callback(null);
      }
      return;
    }

    this.requestsService
      .putData<TaskViewModel>(this.baseUrl, data)
      .pipe(
        catchError(err => {
          this.onError(err, callback);
          return throwError(() => err);
        })
      )
      .subscribe(response => {
        if (response.ok && callback) {
          callback(response.body);
        } else {
          callback(null);
        }
      });
  }

  public updateField(id: number, field: string, data: any, callback: Action<TaskViewModel>) {
    if (!id || !data || !field) {
      if (callback) {
        callback(null);
      }
      return;
    }

    this.requestsService
      .putData<TaskViewModel>([this.baseUrl, id, field].join("/"), {value: data})
      .pipe(
        catchError(err => {
          this.onError(err, callback);
          return throwError(() => err);
        })
      )
      .subscribe(response => {
        if (response.ok && callback) {
          callback(response.body);
        } else {
          callback(null);
        }
      });
  }

  public delete(id: number, callback: Action<number>) {
    if (!id) {
      if (callback) {
        callback(null);
      }
      return;
    }

    this.requestsService
      .deleteData<number>([this.baseUrl, id].join("/"))
      .pipe(
        catchError(err => {
          this.onError(err, callback);
          return throwError(() => err);
        })
      )
      .subscribe(response => {
        if (response.ok && callback) {
          callback(response.body);
        } else {
          callback(null);
        }
      });
  }

  public addLabel(model: TaskViewModel, label: LabelViewModel, callback: Action<boolean>) {
    if (!model || !label) {
      if (callback) {
        callback(null);
      }
      return;
    }

    this.requestsService
      .postData<{id: number}>([this.baseUrl, model.id, "labels", label.id].join("/"), {})
      .pipe(
        catchError(err => {
          this.onError(err, callback);
          return throwError(() => err);
        })
      )
      .subscribe(response => {
        if (response.ok && callback) {
          callback(!!response.body);
        } else {
          callback(null);
        }
      });
  }

  public deleteLabel(model: TaskViewModel, label: LabelViewModel, callback: Action<boolean>) {
    if (!model || !label) {
      if (callback) {
        callback(null);
      }
      return;
    }

    this.requestsService
      .deleteData<{id: number}>([this.baseUrl, model.id, "labels", label.id].join("/"))
      .pipe(
        catchError(err => {
          this.onError(err, callback);
          return throwError(() => err);
        })
      )
      .subscribe(response => {
        if (response.ok && callback) {
          callback(!!response.body);
        } else {
          callback(null);
        }
      });
  }

  public addUser(model: TaskViewModel, user: UserViewModel, callback: Action<boolean>) {
    if (!model || !user) {
      if (callback) {
        callback(null);
      }
      return;
    }

    this.requestsService
      .postData<{id: number}>([this.baseUrl, model.id, "users", user.id].join("/"), {})
      .pipe(
        catchError(err => {
          this.onError(err, callback);
          return throwError(() => err);
        })
      )
      .subscribe(response => {
        if (response.ok && callback) {
          callback(!!response.body);
        } else {
          callback(null);
        }
      });
  }

  public deleteUser(model: TaskViewModel, user: UserViewModel, callback: Action<boolean>) {
    if (!model || !user) {
      if (callback) {
        callback(null);
      }
      return;
    }

    this.requestsService
      .deleteData<{id: number}>([this.baseUrl, model.id, "users", user.id].join("/"))
      .pipe(
        catchError(err => {
          this.onError(err, callback);
          return throwError(() => err);
        })
      )
      .subscribe(response => {
        if (response.ok && callback) {
          callback(!!response.body);
        } else {
          callback(null);
        }
      });
  }

  private onError(error: HttpErrorResponse, callback: any) {
    if (callback) {
      callback(null);
    }
  }
}
