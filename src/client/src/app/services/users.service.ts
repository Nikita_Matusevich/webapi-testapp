﻿import {HttpErrorResponse} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Router} from "@angular/router";
import {catchError, throwError} from "rxjs";

import {Action} from "../interfaces/action.interface";
import {UserViewModel} from "../models/user.model";

import {RequestsService} from "./requests.service";

@Injectable()
export class UsersService {
  private baseUrl: string = "users";

  public constructor(
    private requestsService: RequestsService,
    private router: Router
  ) {}

  public getAll(callback: Action<UserViewModel[]>) {
    this.requestsService
      .getData<UserViewModel[]>(this.baseUrl)
      .pipe(
        catchError(err => {
          this.onError(err, callback);
          return throwError(() => err);
        })
      )
      .subscribe(response => {
        if (response.ok && callback) {
          callback(response.body);
        } else {
          callback(null);
        }
      });
  }

  public getById(id: number, callback: Action<UserViewModel>) {
    if (!id) {
      return;
    }
    this.requestsService
      .getData<UserViewModel>([this.baseUrl, id].join("/"))
      .pipe(
        catchError(err => {
          this.onError(err, callback);
          return throwError(() => err);
        })
      )
      .subscribe(response => {
        if (response.ok && callback) {
          callback(response.body);
        } else {
          callback(null);
        }
      });
  }

  public create(data: UserViewModel, callback: Action<UserViewModel>) {
    if (!data) {
      if (callback) {
        callback(null);
      }
      return;
    }

    this.requestsService
      .postData<UserViewModel>(this.baseUrl, data)
      .pipe(
        catchError(err => {
          this.onError(err, callback);
          return throwError(() => err);
        })
      )
      .subscribe(response => {
        if (response.ok && callback) {
          callback(response.body);
        } else {
          callback(null);
        }
      });
  }

  public update(data: UserViewModel, callback: Action<UserViewModel>) {
    if (!data) {
      if (callback) {
        callback(null);
      }
      return;
    }

    this.requestsService
      .putData<UserViewModel>(this.baseUrl, data)
      .pipe(
        catchError(err => {
          this.onError(err, callback);
          return throwError(() => err);
        })
      )
      .subscribe(response => {
        if (response.ok && callback) {
          callback(response.body);
        } else {
          callback(null);
        }
      });
  }

  public delete(id: number, callback: Action<number>) {
    if (!id) {
      if (callback) {
        callback(null);
      }
      return;
    }

    this.requestsService
      .deleteData<number>([this.baseUrl, id].join("/"))
      .pipe(
        catchError(err => {
          this.onError(err, callback);
          return throwError(() => err);
        })
      )
      .subscribe(response => {
        if (response.ok && callback) {
          callback(response.body);
        } else {
          callback(null);
        }
      });
  }

  private onError(error: HttpErrorResponse, callback: any) {
    if (callback) {
      callback(null);
    }
  }
}
