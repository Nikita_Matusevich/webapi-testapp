﻿CREATE TABLE [dbo].[TaskLabels]
(
	[TaskId] BIGINT NOT NULL,
    [LabelId] BIGINT NOT NULL,

    CONSTRAINT [PK_TaskLabels] PRIMARY KEY CLUSTERED ([TaskId] ASC, [LabelId] ASC),
    CONSTRAINT [FK_TaskLabels_Tasks] FOREIGN KEY ([TaskId]) REFERENCES [dbo].[Tasks] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_TaskLabels_Labels] FOREIGN KEY ([LabelId]) REFERENCES [dbo].[Labels] ([Id]) ON DELETE CASCADE
)

GO

CREATE NONCLUSTERED INDEX [IX_TaskLabels_TaskId]
    ON [dbo].[TaskLabels]([TaskId] ASC);

GO

CREATE NONCLUSTERED INDEX [IX_TaskLabels_LabelId]
    ON [dbo].[TaskLabels]([LabelId] ASC);

GO
