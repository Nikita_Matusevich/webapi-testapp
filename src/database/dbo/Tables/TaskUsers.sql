﻿CREATE TABLE [dbo].[TaskUsers]
(
	[TaskId] BIGINT NOT NULL,
	[UserId] BIGINT NOT NULL,
	
	CONSTRAINT [PK_TaskUsers] PRIMARY KEY CLUSTERED ([TaskId] ASC, [UserId] ASC),
    CONSTRAINT [FK_TaskUsers_Tasks] FOREIGN KEY ([TaskId]) REFERENCES [dbo].[Tasks] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_TaskUsers_Users] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([Id]) ON DELETE CASCADE
)

GO

CREATE NONCLUSTERED INDEX [IX_TaskUsers_TaskId]
    ON [dbo].[TaskUsers]([TaskId] ASC);

GO

CREATE NONCLUSTERED INDEX [IX_TaskUsers_UserId]
    ON [dbo].[TaskUsers]([UserId] ASC);

GO
