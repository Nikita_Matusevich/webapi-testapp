namespace WebApiTestApp.Common.ConfigurationOptions;

public class DatabaseOptions
{
    public const string Path = "Database";

    public string SqlDataSource { get; set; }
    public string SqlDataCatalog { get; set; }
    public string SqlDataUser { get; set; }
    public string SqlDataPassword { get; set; }
}