namespace WebApiTestApp.Common.ConfigurationOptions;

public class GeneralOptions
{
    public const string Path = "General";

    public string GitHash { get; set; }
}
