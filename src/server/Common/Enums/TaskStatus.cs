﻿namespace WebApiTestApp.Common.Enums;

public enum TaskStatus : byte
{
    Undefined = 0,
    Created = 1,
    Started = 2,
    Finished = 4
}