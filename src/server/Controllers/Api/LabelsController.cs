﻿using Microsoft.AspNetCore.Mvc;
using WebApiTestApp.Domain.Models;
using WebApiTestApp.Domain.Services.Interfaces;

namespace WebApiTestApp.Controllers.Api;

[ApiController]
[Route("api/labels")]
public class LabelsController : ControllerBase
{
    private readonly ILabelsService _labelsService;

    public LabelsController(ILabelsService labelsService)
    {
        _labelsService = labelsService;
    }

    [Route(""), HttpGet]
    public ActionResult Get()
    {
        var labels = _labelsService.Get();
        return Ok(labels);
    }

    [Route("{id:long}"), HttpGet]
    public ActionResult GetById(long id)
    {
        var label = _labelsService.GetById(id);
        if (label == null)
        {
            return BadRequest("An error occurred..");
        }

        return Ok(label);
    }

    [Route(""), HttpPost]
    public async System.Threading.Tasks.Task<ActionResult> Create(Label request)
    {
        var label = await _labelsService.CreateAsync(request);
        if (label == null)
        {
            return BadRequest("An error occurred..");
        }

        return Ok(label);
    }

    [Route(""), HttpPut]
    public async System.Threading.Tasks.Task<ActionResult> Update(Label request)
    {
        var label = await _labelsService.UpdateAsync(request);
        if (label == null)
        {
            return BadRequest("An error occurred..");
        }

        return Ok(label);
    }

    [Route("{id:long}"), HttpDelete]
    public async System.Threading.Tasks.Task<ActionResult> Delete(long id)
    {
        var ok = await _labelsService.DeleteAsync(id);
        if (!ok)
        {
            return BadRequest("An error occurred..");
        }

        return Ok(id);
    }
}