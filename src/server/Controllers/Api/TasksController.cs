﻿using Microsoft.AspNetCore.Mvc;
using WebApiTestApp.Domain.Models;
using WebApiTestApp.Domain.Services.Interfaces;
using Task = WebApiTestApp.Domain.Models.Task;
using TaskStatus = WebApiTestApp.Common.Enums.TaskStatus;

namespace WebApiTestApp.Controllers.Api;

[ApiController]
[Route("api/tasks")]
public class TasksController : ControllerBase
{
    private readonly ITasksService _tasksService;

    public TasksController(ITasksService tasksService)
    {
        _tasksService = tasksService;
    }

    [Route(""), HttpGet]
    public ActionResult Get([FromQuery] GetTasksRequest request)
    {
        var tasks = _tasksService.Get(request);
        return Ok(tasks);
    }

    [Route("{id:long}"), HttpGet]
    public ActionResult GetById(long id)
    {
        var task = _tasksService.GetById(id);
        if (task == null)
        {
            return BadRequest("An error occurred..");
        }

        return Ok(task);
    }

    [Route(""), HttpPost]
    public async System.Threading.Tasks.Task<ActionResult> Create(Task request)
    {
        var task = await _tasksService.CreateAsync(request);
        if (task == null)
        {
            return BadRequest("An error occurred..");
        }

        return Ok(task);
    }

    [Route(""), HttpPut]
    public async System.Threading.Tasks.Task<ActionResult> Update(Task request)
    {
        var task = await _tasksService.UpdateAsync(request);
        if (task == null)
        {
            return BadRequest("An error occurred..");
        }

        return Ok(task);
    }

    [Route("{id:long}/title"), HttpPut]
    public async System.Threading.Tasks.Task<ActionResult> UpdateTitle(long id, FieldUpdateRequest<string> request)
    {
        var task = await _tasksService.UpdateTitleAsync(id, request.Value);
        if (task == null)
        {
            return BadRequest("An error occurred..");
        }

        return Ok(task);
    }

    [Route("{id:long}/description"), HttpPut]
    public async System.Threading.Tasks.Task<ActionResult> UpdateDescription(long id, FieldUpdateRequest<string> request)
    {
        var task = await _tasksService.UpdateDescriptionAsync(id, request.Value);
        if (task == null)
        {
            return BadRequest("An error occurred..");
        }

        return Ok(task);
    }

    [Route("{id:long}/status"), HttpPut]
    public async System.Threading.Tasks.Task<ActionResult> UpdateStatus(long id, FieldUpdateRequest<TaskStatus> request)
    {
        var task = await _tasksService.UpdateStatusAsync(id, request.Value);
        if (task == null)
        {
            return BadRequest("An error occurred..");
        }

        return Ok(task);
    }

    [Route("{id:long}/estimatedTime"), HttpPut]
    public async System.Threading.Tasks.Task<ActionResult> UpdateEstimatedTime(long id, FieldUpdateRequest<DateTime> request)
    {
        var task = await _tasksService.UpdateEstimatedTimeAsync(id, request.Value);
        if (task == null)
        {
            return BadRequest("An error occurred..");
        }

        return Ok(task);
    }

    [Route("{id:long}"), HttpDelete]
    public async System.Threading.Tasks.Task<ActionResult> Delete(long id)
    {
        var ok = await _tasksService.DeleteAsync(id);
        if (!ok)
        {
            return BadRequest("An error occurred..");
        }

        return Ok(id);
    }


    [Route("{id:long}/labels/{labelId:long}"), HttpPost]
    public async System.Threading.Tasks.Task<ActionResult> AddLabel(long id, long labelId)
    {
        var ok = await _tasksService.AddLabelAsync(id, labelId);
        if (!ok)
        {
            return BadRequest("An error occurred..");
        }

        return Ok(new {id});
    }

    [Route("{id:long}/labels/{labelId:long}"), HttpDelete]
    public async System.Threading.Tasks.Task<ActionResult> DeleteLabel(long id, long labelId)
    {
        var ok = await _tasksService.DeleteLabelAsync(id, labelId);
        if (!ok)
        {
            return BadRequest("An error occurred..");
        }

        return Ok(new {id});
    }


    [Route("{id:long}/users/{userId:long}"), HttpPost]
    public async System.Threading.Tasks.Task<ActionResult> AddUser(long id, long userId)
    {
        var ok = await _tasksService.AddUserAsync(id, userId);
        if (!ok)
        {
            return BadRequest("An error occurred..");
        }

        return Ok(new {id});
    }

    [Route("{id:long}/users/{userId:long}"), HttpDelete]
    public async System.Threading.Tasks.Task<ActionResult> DeleteUser(long id, long userId)
    {
        var ok = await _tasksService.DeleteUserAsync(id, userId);
        if (!ok)
        {
            return BadRequest("An error occurred..");
        }

        return Ok(new {id});
    }
}