﻿using Microsoft.AspNetCore.Mvc;
using WebApiTestApp.Domain.Models;
using WebApiTestApp.Domain.Services.Interfaces;

namespace WebApiTestApp.Controllers.Api;

[ApiController]
[Route("api/users")]
public class UsersController : ControllerBase
{
    private readonly IUsersService _usersService;

    public UsersController(IUsersService usersService)
    {
        _usersService = usersService;
    }

    [Route(""), HttpGet]
    public ActionResult Get()
    {
        var users = _usersService.Get();
        return Ok(users);
    }

    [Route("{id:long}"), HttpGet]
    public ActionResult GetById(long id)
    {
        var user = _usersService.GetById(id);
        if (user == null)
        {
            return BadRequest("An error occurred..");
        }

        return Ok(user);
    }

    [Route(""), HttpPost]
    public async System.Threading.Tasks.Task<ActionResult> Create(User request)
    {
        var user = await _usersService.CreateAsync(request);
        if (user == null)
        {
            return BadRequest("An error occurred..");
        }

        return Ok(user);
    }

    [Route(""), HttpPut]
    public async System.Threading.Tasks.Task<ActionResult> Update(User request)
    {
        var user = await _usersService.UpdateAsync(request);
        if (user == null)
        {
            return BadRequest("An error occurred..");
        }

        return Ok(user);
    }

    [Route("{id:long}"), HttpDelete]
    public async System.Threading.Tasks.Task<ActionResult> Delete(long id)
    {
        var ok = await _usersService.DeleteAsync(id);
        if (!ok)
        {
            return BadRequest("An error occurred..");
        }

        return Ok(id);
    }
}