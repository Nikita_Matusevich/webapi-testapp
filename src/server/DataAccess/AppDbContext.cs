﻿using Microsoft.EntityFrameworkCore;

namespace WebApiTestApp.DataAccess;

public partial class AppDbContext : DbContext
{
    public AppDbContext()
    {
    }

    public AppDbContext(DbContextOptions<AppDbContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Models.Label> Labels { get; set; }
    public virtual DbSet<Models.Task> Tasks { get; set; }
    public virtual DbSet<Models.User> Users { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Models.Label>(entity =>
        {
            entity.Property(e => e.HexColor)
                .IsRequired()
                .HasMaxLength(10);

            entity.Property(e => e.Title)
                .IsRequired()
                .HasMaxLength(100);
        });

        modelBuilder.Entity<Models.Task>(entity =>
        {
            entity.Property(e => e.DateCreated).HasColumnType("datetime");

            entity.Property(e => e.Description).HasMaxLength(2000);

            entity.Property(e => e.EstimatedTime).HasColumnType("datetime");

            entity.Property(e => e.Title)
                .IsRequired()
                .HasMaxLength(256);

            entity.HasMany(d => d.Labels)
                .WithMany(p => p.Tasks)
                .UsingEntity<Dictionary<string, object>>(
                    "TaskLabel",
                    l => l.HasOne<Models.Label>().WithMany().HasForeignKey("LabelId").HasConstraintName("FK_TaskLabels_Labels"),
                    r => r.HasOne<Models.Task>().WithMany().HasForeignKey("TaskId").HasConstraintName("FK_TaskLabels_Tasks"),
                    j =>
                    {
                        j.HasKey("TaskId", "LabelId");

                        j.ToTable("TaskLabels");

                        j.HasIndex(new[] { "LabelId" }, "IX_TaskLabels_LabelId");

                        j.HasIndex(new[] { "TaskId" }, "IX_TaskLabels_TaskId");
                    });

            entity.HasMany(d => d.Users)
                .WithMany(p => p.Tasks)
                .UsingEntity<Dictionary<string, object>>(
                    "TaskUser",
                    l => l.HasOne<Models.User>().WithMany().HasForeignKey("UserId").HasConstraintName("FK_TaskUsers_Users"),
                    r => r.HasOne<Models.Task>().WithMany().HasForeignKey("TaskId").HasConstraintName("FK_TaskUsers_Tasks"),
                    j =>
                    {
                        j.HasKey("TaskId", "UserId");

                        j.ToTable("TaskUsers");

                        j.HasIndex(new[] { "TaskId" }, "IX_TaskUsers_TaskId");

                        j.HasIndex(new[] { "UserId" }, "IX_TaskUsers_UserId");
                    });
        });

        modelBuilder.Entity<Models.User>(entity =>
        {
            entity.Property(e => e.DateCreated).HasColumnType("datetime");

            entity.Property(e => e.Email)
                .IsRequired()
                .HasMaxLength(100);

            entity.Property(e => e.FirstName)
                .IsRequired()
                .HasMaxLength(100);

            entity.Property(e => e.LastName)
                .IsRequired()
                .HasMaxLength(100);

            entity.Property(e => e.Position).HasMaxLength(100);
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}