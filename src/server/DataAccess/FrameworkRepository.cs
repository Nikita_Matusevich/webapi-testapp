﻿using Microsoft.EntityFrameworkCore;
using WebApiTestApp.DataAccess.Repositories;

namespace WebApiTestApp.DataAccess
{
    public class AppDbContextFrameworkRepository : EntityFrameworkRepository<DbContext>, IApplicationRepository
    {
        public AppDbContextFrameworkRepository(AppDbContext context) : base(context)
        {
        }
    }
}