﻿using WebApiTestApp.DataAccess.Repositories;

namespace WebApiTestApp.DataAccess.Models;

public partial class Label : Entity<long>
{
    public Label()
    {
        Tasks = new HashSet<Task>();
    }

    public string Title { get; set; }
    public string HexColor { get; set; }

    public virtual ICollection<Task> Tasks { get; set; }
}