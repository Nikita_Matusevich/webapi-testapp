﻿using WebApiTestApp.DataAccess.Repositories;
using TaskStatus = WebApiTestApp.Common.Enums.TaskStatus;

namespace WebApiTestApp.DataAccess.Models;

public partial class Task : Entity<long>
{
    public Task()
    {
        Labels = new HashSet<Label>();
        Users = new HashSet<User>();
    }

    public string Title { get; set; }
    public string Description { get; set; }
    public DateTime DateCreated { get; set; }
    public DateTime? EstimatedTime { get; set; }
    public TaskStatus Status { get; set; }

    public virtual ICollection<Label> Labels { get; set; }
    public virtual ICollection<User> Users { get; set; }
}