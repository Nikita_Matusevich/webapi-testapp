﻿using WebApiTestApp.DataAccess.Repositories;

namespace WebApiTestApp.DataAccess.Models;

public partial class User : Entity<long>
{
    public User()
    {
        Tasks = new HashSet<Task>();
    }

    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Position { get; set; }
    public DateTime DateCreated { get; set; }
    public string Email { get; set; }

    public virtual ICollection<Task> Tasks { get; set; }
}