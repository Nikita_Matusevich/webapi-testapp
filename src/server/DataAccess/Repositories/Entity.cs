﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApiTestApp.DataAccess.Repositories;

public abstract class Entity<T> : IEntity<T>
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public T Id { get; set; }
    object IEntity.Id
    {
        get => Id;
        set => Id = (T)value;
    }

    /* [Required] // TODO: return these lines later so we can track all the updates
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public DateTime CreatedAt { get; set; }
    DateTime IEntity.CreatedAt
    {
        get => CreatedAt;
        set => CreatedAt = value;
    }

    [Required]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public DateTime UpdatedAt { get; set; }
    DateTime IEntity.UpdatedAt
    {
        get => UpdatedAt;
        set => UpdatedAt = value;
    }*/

    public bool HasProperty(string propertyName)
    {
        return GetType().GetProperty(propertyName) != null;
    }

    public bool SetProperty(string propertyName, object value)
    {
        try
        {
            var property = GetType().GetProperty(propertyName);
            property?.SetValue(this, value);

            return true;
        }
        catch
        {
            return false;
        }
    }
}