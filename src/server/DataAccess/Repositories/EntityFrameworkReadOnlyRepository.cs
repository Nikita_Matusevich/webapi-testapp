using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

namespace WebApiTestApp.DataAccess.Repositories;

public class EntityFrameworkReadOnlyRepository<TContext> : IReadOnlyRepository
    where TContext : DbContext
{
    protected readonly TContext Context;

    public EntityFrameworkReadOnlyRepository(TContext context)
    {
        Context = context;
    }

    protected virtual IQueryable<TEntity> GetQueryable<TEntity>(
        Expression<Func<TEntity, bool>>? filter = null,
        Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null,
        string? includeProperties = null,
        int? skip = null,
        int? take = null,
        bool? asNoTracking = false,
        Expression<Func<TEntity, TEntity>>? select = null)
        where TEntity : class, IEntity
    {
        includeProperties ??= string.Empty;
        IQueryable<TEntity> query = Context.Set<TEntity>();

        if (filter != null)
        {
            query = query.Where(filter);

        }

        if (includeProperties.Contains("*"))
        {
            foreach (var property in typeof(TEntity).GetProperties())
            {
                if (property.PropertyType.IsGenericType && typeof(ICollection<>).IsAssignableFrom(property.PropertyType.GetGenericTypeDefinition()))
                {
                    query = query.Include(property.Name);
                }
            }
        }
        else
        {
            foreach (var includeProperty in includeProperties.Split
                        (new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty.Trim());
            }
        }

        if (orderBy != null)
        {
            query = orderBy(query);
        }

        if (skip.HasValue)
        {
            query = query.Skip(skip.Value);
        }

        if (take.HasValue)
        {
            query = query.Take(take.Value);
        }

        if (asNoTracking.HasValue && asNoTracking.Value)
        {
            Context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        if (select != null)
        {
            query = query.Select(select);
        }

        //try
        //{
        //    Debug.Print("EntityFrameworkReadOnlyRepository - GetQueryable: " + query.ToQueryString());
        //}
        //catch (Exception){}

        return query;
    }

    public virtual IEnumerable<TEntity> GetAll<TEntity>(
        Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null,
        string? includeProperties = null,
        int? skip = null,
        int? take = null,
        bool? asNoTracking = false)
        where TEntity : class, IEntity
    {
        return GetQueryable(null, orderBy, includeProperties, skip, take).ToList();
    }

    public virtual async Task<IEnumerable<TEntity>> GetAllAsync<TEntity>(
        Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null,
        string? includeProperties = null,
        int? skip = null,
        int? take = null,
        bool? asNoTracking = false)
        where TEntity : class, IEntity
    {
        return await GetQueryable(null, orderBy, includeProperties, skip, take).ToListAsync();
    }

    public virtual IEnumerable<TEntity> Get<TEntity>(
        Expression<Func<TEntity, bool>>? filter = null,
        Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null,
        string? includeProperties = null,
        int? skip = null,
        int? take = null,
        bool? asNoTracking = false,
        Expression<Func<TEntity, TEntity>>? select = null)
        where TEntity : class, IEntity
    {
        return GetQueryable(filter, orderBy, includeProperties, skip, take, asNoTracking, select).ToList();
    }

    public virtual async Task<IEnumerable<TEntity>> GetAsync<TEntity>(
        Expression<Func<TEntity, bool>>? filter = null,
        Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null,
        string? includeProperties = null,
        int? skip = null,
        int? take = null,
        bool? asNoTracking = false,
        Expression<Func<TEntity, TEntity>>? select = null)
        where TEntity : class, IEntity
    {
        return await GetQueryable(filter, orderBy, includeProperties, skip, take, asNoTracking, select).ToListAsync();
    }

    public virtual TEntity? GetOne<TEntity>(
        Expression<Func<TEntity, bool>>? filter = null,
        string? includeProperties = "",
        bool? asNoTracking = false)
        where TEntity : class, IEntity
    {
        return GetQueryable(filter, null, includeProperties, asNoTracking: asNoTracking).SingleOrDefault();
    }

    public virtual async Task<TEntity?> GetOneAsync<TEntity>(
        Expression<Func<TEntity, bool>>? filter = null,
        string? includeProperties = null)
        where TEntity : class, IEntity
    {
        return await GetQueryable(filter, null, includeProperties).SingleOrDefaultAsync();
    }

    public virtual TEntity? GetFirst<TEntity>(
        Expression<Func<TEntity, bool>>? filter = null,
        Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null,
        string? includeProperties = "",
        Expression<Func<TEntity, TEntity>>? select = null)
        where TEntity : class, IEntity
    {
        return GetQueryable(filter, orderBy, includeProperties, select: select).FirstOrDefault();
    }

    public virtual async Task<TEntity?> GetFirstAsync<TEntity>(
        Expression<Func<TEntity, bool>>? filter = null,
        Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null,
        string? includeProperties = null,
        Expression<Func<TEntity, TEntity>>? select = null)
        where TEntity : class, IEntity
    {
        return await GetQueryable(filter, orderBy, includeProperties, select: select).FirstOrDefaultAsync();
    }

    public virtual TEntity? GetById<TEntity>(object id)
        where TEntity : class, IEntity
    {
        return Context.Set<TEntity>().Find(id);
    }

    public virtual async Task<TEntity?> GetByIdAsync<TEntity>(object id)
        where TEntity : class, IEntity
    {
        return await Context.Set<TEntity>().FindAsync(id);
    }

    public virtual int GetCount<TEntity>(Expression<Func<TEntity, bool>>? filter = null)
        where TEntity : class, IEntity
    {
        return GetQueryable(filter).Count();
    }

    public virtual Task<int> GetCountAsync<TEntity>(Expression<Func<TEntity, bool>>? filter = null)
        where TEntity : class, IEntity
    {
        return GetQueryable(filter).CountAsync();
    }

    public virtual bool GetExists<TEntity>(Expression<Func<TEntity, bool>>? filter = null)
        where TEntity : class, IEntity
    {
        return GetQueryable(filter).Any();
    }

    public virtual Task<bool> GetExistsAsync<TEntity>(Expression<Func<TEntity, bool>>? filter = null)
        where TEntity : class, IEntity
    {
        return GetQueryable(filter).AnyAsync();
    }
}