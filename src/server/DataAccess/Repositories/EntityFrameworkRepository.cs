﻿using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

namespace WebApiTestApp.DataAccess.Repositories;

public class EntityFrameworkRepository<TContext> : EntityFrameworkReadOnlyRepository<TContext>, IRepository
    where TContext : DbContext
{
    public EntityFrameworkRepository(TContext context)
        : base(context)
    {
    }

    public virtual void Create<TEntity>(TEntity entity, string? createdBy = null)
        where TEntity : class, IEntity
    {
        if (entity.HasProperty("CreatedAt"))
        {
            entity.SetProperty("CreatedAt", DateTime.UtcNow);
        }

        if (entity.HasProperty("UpdatedAt"))
        {
            entity.SetProperty("UpdatedAt", DateTime.UtcNow);
        }

        Context.Set<TEntity>().Add(entity);
    }

    public virtual void Update<TEntity>(TEntity entity, string? modifiedBy = null)
        where TEntity : class, IEntity
    {
        if (entity.HasProperty("UpdatedAt"))
        {
            entity.SetProperty("UpdatedAt", DateTime.UtcNow);
        }

        Context.Set<TEntity>().Attach(entity);
        Context.Entry(entity).State = EntityState.Modified;
    }

    public void UpdateProperty<TEntity, TProperty>(
        TEntity entity,
        Expression<Func<TEntity, TProperty>> propertyExpression,
        string? modifiedBy = null
    )
        where TEntity : class, IEntity
    {
        if (entity == null) throw new ArgumentNullException();

        Context.Set<TEntity>()
            .Attach(entity)
            .Property(propertyExpression)
            .IsModified = true;
    }

    public virtual void Delete<TEntity>(object? id)
        where TEntity : class, IEntity
    {
        var entity = Context.Set<TEntity>().Find(id);
        if (entity != null)
            Delete(entity);
    }

    public virtual void Delete<TEntity>(TEntity entity)
        where TEntity : class, IEntity
    {
        var dbSet = Context.Set<TEntity>();
        if (Context.Entry(entity).State == EntityState.Detached)
        {
            dbSet.Attach(entity);
        }
        dbSet.Remove(entity);
    }

    public virtual void DeleteAt<TEntity>(object[] ids)
        where TEntity : class, IEntity
    {
        var dbSet = Context.Set<TEntity>();
        dbSet.RemoveRange(dbSet.Where(x => ids.Contains(x.Id)));
    }

    public virtual void Save()
    {
        try
        {
            Context.SaveChanges();
        }
        catch (ValidationException e)
        {
            throw new ValidationException(e.Message, e.InnerException);
        }
    }

    public virtual Task SaveAsync()
    {
        try
        {
            return Context.SaveChangesAsync();
        }
        catch (ValidationException e)
        {
            throw new ValidationException(e.Message, e.InnerException);
        }
    }

    public IRepositoryTransaction BeginTransaction()
    {
        return new EntityFrameworkTransaction(Context);
    }
}