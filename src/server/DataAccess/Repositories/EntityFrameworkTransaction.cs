﻿using Microsoft.EntityFrameworkCore;

namespace WebApiTestApp.DataAccess.Repositories;

public sealed class EntityFrameworkTransaction : IRepositoryTransaction
{
    private readonly DbContext _context;

    public EntityFrameworkTransaction(DbContext context)
    {
        _context = context ?? throw new ArgumentNullException(nameof(context));
    }

    public async Task ExecuteAsync(Func<Task> action)
    {
        if (action == null)
        {
            throw new ArgumentNullException(nameof(action));
        }

        var strategy = _context.Database.CreateExecutionStrategy();
        await strategy.ExecuteAsync(async () =>
        {
            await using var transaction = await _context.Database.BeginTransactionAsync();
            await action();
            await transaction.CommitAsync();
        });
    }
}
