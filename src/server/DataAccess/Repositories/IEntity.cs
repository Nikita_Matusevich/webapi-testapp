﻿namespace WebApiTestApp.DataAccess.Repositories;

public interface IEntity<T> : IEntity
{
    new T Id { get; set; }
}

public interface IEntity
{
    object Id { get; set; }

    // public DateTime CreatedAt { get; set; } TODO: return these lines later, so we can track all updates of all the entities
    // public DateTime UpdatedAt { get; set; }

    bool HasProperty(string propertyName);

    bool SetProperty(string propertyName, object value);
}