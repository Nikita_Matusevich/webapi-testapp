﻿using System.Linq.Expressions;

namespace WebApiTestApp.DataAccess.Repositories;

public interface IRepository : IReadOnlyRepository
{
    IRepositoryTransaction BeginTransaction();

    void Create<TEntity>(TEntity entity, string? createdBy = null)
        where TEntity : class, IEntity;

    void Update<TEntity>(TEntity entity, string? modifiedBy = null)
        where TEntity : class, IEntity;

    void UpdateProperty<TEntity, TProperty>(TEntity entity, Expression<Func<TEntity, TProperty>> propertyExpression, string? modifiedBy = null)
        where TEntity : class, IEntity;

    void Delete<TEntity>(object? id)
        where TEntity : class, IEntity;

    void Delete<TEntity>(TEntity entity)
        where TEntity : class, IEntity;

    void DeleteAt<TEntity>(object[] ids)
        where TEntity : class, IEntity;

    void Save();

    Task SaveAsync();
}
