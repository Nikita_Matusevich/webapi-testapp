﻿namespace WebApiTestApp.DataAccess.Repositories;

public interface IRepositoryTransaction
{
    Task ExecuteAsync(Func<Task> action);
}
