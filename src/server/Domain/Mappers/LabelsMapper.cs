using AutoMapper;

internal class LabelsMapper : Profile
{
    public LabelsMapper()
    {
        CreateMap<WebApiTestApp.DataAccess.Models.Label, WebApiTestApp.Domain.Models.Label>().MaxDepth(2);
        CreateMap<WebApiTestApp.Domain.Models.Label, WebApiTestApp.DataAccess.Models.Label>();
    }
}