using AutoMapper;

internal class TasksMapper : Profile
{
    public TasksMapper()
    {
        CreateMap<WebApiTestApp.DataAccess.Models.Task, WebApiTestApp.Domain.Models.Task>().MaxDepth(2);
        CreateMap<WebApiTestApp.Domain.Models.Task, WebApiTestApp.DataAccess.Models.Task>();
    }
}