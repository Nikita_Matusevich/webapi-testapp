using AutoMapper;

internal class UsersMapper : Profile
{
    public UsersMapper()
    {
        CreateMap<WebApiTestApp.DataAccess.Models.User, WebApiTestApp.Domain.Models.User>().MaxDepth(2);
        CreateMap<WebApiTestApp.Domain.Models.User, WebApiTestApp.DataAccess.Models.User>();
    }
}