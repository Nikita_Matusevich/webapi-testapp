﻿namespace WebApiTestApp.Domain.Models;

public class FieldUpdateRequest<T>
{
    public T Value { get; set; }
}