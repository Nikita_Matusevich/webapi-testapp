﻿namespace WebApiTestApp.Domain.Models;

public class GetTasksRequest
{
    public string? Title { get; set; }
}