namespace WebApiTestApp.Domain.Models;

public class Label
{
    public long Id { get; set; }

    public string Title { get; set; }

    public string HexColor { get; set; }
}