using TaskStatus = WebApiTestApp.Common.Enums.TaskStatus;

namespace WebApiTestApp.Domain.Models;

public class Task
{
    public Task()
    {
        Labels = new List<Label>();
        Users = new List<User>();
    }

    public long Id { get; set; }

    public string Title { get; set; }

    public string Description { get; set; }

    public DateTime DateCreated { get; set; }

    public DateTime? EstimatedTime { get; set; }

    public TaskStatus Status { get; set; }

    public ICollection<Label> Labels { get; set; }

    public ICollection<User> Users { get; set; }
}