namespace WebApiTestApp.Domain.Models;

public class User
{
    public long Id { get; set; }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    public string Position { get; set; }

    public string Email { get; set; }
}