﻿using AutoMapper;
using WebApiTestApp.DataAccess.Repositories;

using WebApiTestApp.Domain.Models;
using WebApiTestApp.Domain.Services.Interfaces;
using Db = WebApiTestApp.DataAccess.Models;

namespace WebApiTestApp.Domain.Services.Implementations;

public class LabelsService : ILabelsService
{
    private readonly IRepository _repository;
    private readonly IMapper _mapper;

    public LabelsService(IRepository repository, IMapper mapper)
    {
        _repository = repository;
        _mapper = mapper;
    }

    public IEnumerable<Label> Get()
    {
        var entities = _repository.Get<Db.Label>(
            orderBy: x => x.OrderBy(order => order.Id),
            includeProperties: "Tasks"
        );

        return _mapper.Map<IEnumerable<Label>>(entities);
    }

    public Label GetById(long id)
    {
        var entity = _repository.GetOne<Db.Label>(
            x => x.Id == id,
            includeProperties: "Tasks"
        );

        return _mapper.Map<Label>(entity);
    }

    public Label Create(Label label)
    {
        if (string.IsNullOrWhiteSpace(label.Title) || !string.IsNullOrWhiteSpace(label.HexColor))
        {
            return null;
        }

        var entity = new Db.Label
        {
            Title = label.Title,
            HexColor = label.HexColor
        };

        _repository.Create(entity);
        _repository.Save();

        return _mapper.Map<Label>(entity);
    }

    public Label Update(Label label)
    {
        if (string.IsNullOrWhiteSpace(label.Title) || string.IsNullOrWhiteSpace(label.HexColor))
        {
            return null;
        }

        var entity = _repository.GetOne<Db.Label>(
            x => x.Id == label.Id,
            includeProperties: "Tasks"
        );
        if (entity == null)
        {
            return null;
        }

        entity.Title = label.Title;
        entity.HexColor = label.HexColor;

        _repository.Update(entity);
        _repository.Save();

        return _mapper.Map<Label>(entity);
    }

    public bool Delete(long id)
    {
        var entity = _repository.GetOne<Db.Label>(
            x => x.Id == id
        );
        if (entity == null)
        {
            return false;
        }

        _repository.Delete(entity);
        _repository.Save();

        return true;
    }

    public async System.Threading.Tasks.Task<Label> CreateAsync(Label label)
    {
        if (string.IsNullOrWhiteSpace(label.Title) || string.IsNullOrWhiteSpace(label.HexColor))
        {
            return null;
        }

        var entity = new Db.Label
        {
            Title = label.Title,
            HexColor = label.HexColor
        };

        _repository.Create(entity);
        await _repository.SaveAsync();

        return _mapper.Map<Label>(entity);
    }

    public async System.Threading.Tasks.Task<Label> UpdateAsync(Label label)
    {
        if (string.IsNullOrWhiteSpace(label.Title) || string.IsNullOrWhiteSpace(label.HexColor))
        {
            return null;
        }

        var entity = _repository.GetOne<Db.Label>(
            x => x.Id == label.Id,
            includeProperties: "Tasks"
        );
        if (entity == null)
        {
            return null;
        }

        entity.Title = label.Title;
        entity.HexColor = label.HexColor;

        _repository.Update(entity);
        await _repository.SaveAsync();

        return _mapper.Map<Label>(entity);
    }

    public async System.Threading.Tasks.Task<bool> DeleteAsync(long id)
    {
        var entity = _repository.GetOne<Db.Label>(
            x => x.Id == id
        );
        if (entity == null)
        {
            return false;
        }

        _repository.Delete(entity);
        await _repository.SaveAsync();

        return true;
    }
}