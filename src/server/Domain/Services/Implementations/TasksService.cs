﻿using AutoMapper;
using WebApiTestApp.DataAccess.Repositories;

using WebApiTestApp.Domain.Models;
using WebApiTestApp.Domain.Services.Interfaces;
using Db = WebApiTestApp.DataAccess.Models;
using Task = WebApiTestApp.Domain.Models.Task;
using TaskStatus = WebApiTestApp.Common.Enums.TaskStatus;

namespace WebApiTestApp.Domain.Services.Implementations;

public class TasksService : ITasksService
{
    private readonly IRepository _repository;
    private readonly IMapper _mapper;

    public TasksService(IRepository repository, IMapper mapper)
    {
        _repository = repository;
        _mapper = mapper;
    }

    public IEnumerable<Task> Get(GetTasksRequest request)
    {
        var entities = string.IsNullOrWhiteSpace(request?.Title)
            ? _repository.Get<Db.Task>(
                orderBy: x => x.OrderBy(order => order.Id),
                includeProperties: "Users, Labels"
            )
            : _repository.Get<Db.Task>(
                filter: x => x.Title == request.Title,
                orderBy: x => x.OrderBy(order => order.Id),
                includeProperties: "Users, Labels"
            );

        return _mapper.Map<IEnumerable<Task>>(entities);
    }

    public Task GetById(long id)
    {
        var entity = _repository.GetOne<Db.Task>(
            x => x.Id == id,
            includeProperties: "Users, Labels"
        );

        return _mapper.Map<Task>(entity);
    }

    public Task Create(Task task)
    {
        if (string.IsNullOrWhiteSpace(task.Title))
        {
            return null;
        }

        var entity = new Db.Task
        {
            Title = task.Title,
            DateCreated = DateTime.UtcNow,
            Description = task.Description,
            EstimatedTime = task.EstimatedTime,
            Status = task.Status != TaskStatus.Undefined
                ? task.Status
                : TaskStatus.Created
        };

        _repository.Create(entity);
        _repository.Save();

        return _mapper.Map<Task>(entity);
    }

    public Task Update(Task task)
    {
        if (string.IsNullOrWhiteSpace(task.Title))
        {
            return null;
        }

        var entity = _repository.GetOne<Db.Task>(
            x => x.Id == task.Id,
            includeProperties: "Users, Labels"
        );
        if (entity == null)
        {
            return null;
        }

        entity.Title = task.Title;
        entity.Description = task.Description;
        entity.EstimatedTime = task.EstimatedTime;
        entity.Status = task.Status != TaskStatus.Undefined
            ? task.Status
            : TaskStatus.Created;

        _repository.Update(entity);
        _repository.Save();

        return _mapper.Map<Task>(entity);
    }

    public bool Delete(long id)
    {
        var entity = _repository.GetOne<Db.Task>(
            x => x.Id == id
        );
        if (entity == null)
        {
            return false;
        }

        _repository.Delete(entity);
        _repository.Save();

        return true;
    }

    public bool AddLabel(long id, long labelId)
    {
        var entity = _repository.GetOne<Db.Task>(
            x => x.Id == id,
            includeProperties: "Labels"
        );
        if (entity == null)
        {
            return false;
        }
        if (entity.Labels.Any(x => x.Id == labelId))
        {
            return true;
        }

        var label = _repository.GetOne<Db.Label>(x => x.Id == labelId);
        if (label == null)
        {
            return false;
        }

        entity.Labels.Add(label);

        _repository.Update(entity);
        _repository.Save();

        return true;
    }

    public bool DeleteLabel(long id, long labelId)
    {
        var entity = _repository.GetOne<Db.Task>(
            x => x.Id == id,
            includeProperties: "Labels"
        );
        if (entity == null)
        {
            return false;
        }
        if (entity.Labels.All(x => x.Id != labelId))
        {
            return true;
        }

        var label = _repository.GetOne<Db.Label>(x => x.Id == labelId);
        if (label == null)
        {
            return false;
        }

        entity.Labels.Remove(label);

        _repository.Update(entity);
        _repository.Save();

        return true;
    }

    public bool AddUser(long id, long userId)
    {
        var entity = _repository.GetOne<Db.Task>(
            x => x.Id == id,
            includeProperties: "Users"
        );
        if (entity == null)
        {
            return false;
        }
        if (entity.Users.Any(x => x.Id == userId))
        {
            return true;
        }

        var user = _repository.GetOne<Db.User>(x => x.Id == userId);
        if (user == null)
        {
            return false;
        }

        entity.Users.Add(user);

        _repository.Update(entity);
        _repository.Save();

        return true;
    }

    public bool DeleteUser(long id, long userId)
    {
        var entity = _repository.GetOne<Db.Task>(
            x => x.Id == id,
            includeProperties: "Users"
        );
        if (entity == null)
        {
            return false;
        }
        if (entity.Users.All(x => x.Id != userId))
        {
            return true;
        }

        var user = _repository.GetOne<Db.User>(x => x.Id == userId);
        if (user == null)
        {
            return false;
        }

        entity.Users.Remove(user);

        _repository.Update(entity);
        _repository.Save();

        return true;
    }

    public async System.Threading.Tasks.Task<Task> CreateAsync(Task task)
    {
        if (string.IsNullOrWhiteSpace(task.Title))
        {
            return null;
        }

        var entity = new Db.Task
        {
            Title = task.Title,
            DateCreated = DateTime.UtcNow,
            Description = task.Description,
            EstimatedTime = task.EstimatedTime,
            Status = task.Status != TaskStatus.Undefined
                ? task.Status
                : TaskStatus.Created
        };

        _repository.Create(entity);
        await _repository.SaveAsync();

        return _mapper.Map<Task>(entity);
    }

    public async System.Threading.Tasks.Task<Task> UpdateAsync(Task task)
    {
        if (string.IsNullOrWhiteSpace(task.Title))
        {
            return null;
        }

        var entity = _repository.GetOne<Db.Task>(
            x => x.Id == task.Id,
            includeProperties: "Users, Labels"
        );
        if (entity == null)
        {
            return null;
        }

        entity.Title = task.Title;
        entity.Description = task.Description;
        entity.EstimatedTime = task.EstimatedTime;
        entity.Status = task.Status != TaskStatus.Undefined
            ? task.Status
            : TaskStatus.Created;

        _repository.Update(entity);
        await _repository.SaveAsync();

        return _mapper.Map<Task>(entity);
    }

    public async System.Threading.Tasks.Task<Task> UpdateTitleAsync(long id, string title)
    {
        if (string.IsNullOrWhiteSpace(title))
        {
            return null;
        }

        var entity = _repository.GetOne<Db.Task>(
            x => x.Id == id,
            includeProperties: "Users, Labels"
        );
        if (entity == null)
        {
            return null;
        }

        entity.Title = title;

        _repository.Update(entity);
        await _repository.SaveAsync();

        return _mapper.Map<Task>(entity);
    }

    public async System.Threading.Tasks.Task<Task> UpdateDescriptionAsync(long id, string description)
    {
        if (string.IsNullOrWhiteSpace(description))
        {
            return null;
        }

        var entity = _repository.GetOne<Db.Task>(
            x => x.Id == id,
            includeProperties: "Users, Labels"
        );
        if (entity == null)
        {
            return null;
        }

        entity.Description = description;

        _repository.Update(entity);
        await _repository.SaveAsync();

        return _mapper.Map<Task>(entity);
    }

    public async System.Threading.Tasks.Task<Task> UpdateStatusAsync(long id, TaskStatus status)
    {
        if (status == TaskStatus.Undefined)
        {
            return null;
        }

        var entity = _repository.GetOne<Db.Task>(
            x => x.Id == id,
            includeProperties: "Users, Labels"
        );
        if (entity == null)
        {
            return null;
        }

        entity.Status = status;

        _repository.Update(entity);
        await _repository.SaveAsync();

        return _mapper.Map<Task>(entity);
    }

    public async System.Threading.Tasks.Task<Task> UpdateEstimatedTimeAsync(long id, DateTime estimatedTime)
    {
        var entity = _repository.GetOne<Db.Task>(
            x => x.Id == id,
            includeProperties: "Users, Labels"
        );
        if (entity == null)
        {
            return null;
        }

        entity.EstimatedTime = estimatedTime;

        _repository.Update(entity);
        await _repository.SaveAsync();

        return _mapper.Map<Task>(entity);
    }

    public async System.Threading.Tasks.Task<bool> DeleteAsync(long id)
    {
        var entity = _repository.GetOne<Db.Task>(
            x => x.Id == id
        );
        if (entity == null)
        {
            return false;
        }

        _repository.Delete(entity);
        await _repository.SaveAsync();

        return true;
    }

    public async System.Threading.Tasks.Task<bool> AddLabelAsync(long id, long labelId)
    {
        var entity = _repository.GetOne<Db.Task>(
            x => x.Id == id,
            includeProperties: "Labels"
        );
        if (entity == null)
        {
            return false;
        }
        if (entity.Labels.Any(x => x.Id == labelId))
        {
            return true;
        }

        var label = _repository.GetOne<Db.Label>(x => x.Id == labelId);
        if (label == null)
        {
            return false;
        }

        entity.Labels.Add(label);

        _repository.Update(entity);
        await _repository.SaveAsync();

        return true;
    }

    public async System.Threading.Tasks.Task<bool> DeleteLabelAsync(long id, long labelId)
    {
        var entity = _repository.GetOne<Db.Task>(
            x => x.Id == id,
            includeProperties: "Labels"
        );
        if (entity == null)
        {
            return false;
        }
        if (entity.Labels.All(x => x.Id != labelId))
        {
            return true;
        }

        var label = _repository.GetOne<Db.Label>(x => x.Id == labelId);
        if (label == null)
        {
            return false;
        }

        entity.Labels.Remove(label);

        _repository.Update(entity);
        await _repository.SaveAsync();

        return true;
    }

    public async System.Threading.Tasks.Task<bool> AddUserAsync(long id, long userId)
    {
        var entity = _repository.GetOne<Db.Task>(
            x => x.Id == id,
            includeProperties: "Users"
        );
        if (entity == null)
        {
            return false;
        }
        if (entity.Users.Any(x => x.Id == userId))
        {
            return true;
        }

        var user = _repository.GetOne<Db.User>(x => x.Id == userId);
        if (user == null)
        {
            return false;
        }

        entity.Users.Add(user);

        _repository.Update(entity);
        await _repository.SaveAsync();

        return true;
    }

    public async System.Threading.Tasks.Task<bool> DeleteUserAsync(long id, long userId)
    {
        var entity = _repository.GetOne<Db.Task>(
            x => x.Id == id,
            includeProperties: "Users"
        );
        if (entity == null)
        {
            return false;
        }
        if (entity.Users.All(x => x.Id != userId))
        {
            return true;
        }

        var user = _repository.GetOne<Db.User>(x => x.Id == userId);
        if (user == null)
        {
            return false;
        }

        entity.Users.Remove(user);

        _repository.Update(entity);
        await _repository.SaveAsync();

        return true;
    }
}