﻿using AutoMapper;
using WebApiTestApp.DataAccess.Repositories;

using WebApiTestApp.Domain.Models;
using WebApiTestApp.Domain.Services.Interfaces;
using Db = WebApiTestApp.DataAccess.Models;

namespace WebApiTestApp.Domain.Services.Implementations;

public class UsersService : IUsersService
{
    private readonly IRepository _repository;
    private readonly IMapper _mapper;

    public UsersService(IRepository repository, IMapper mapper)
    {
        _repository = repository;
        _mapper = mapper;
    }

    public IEnumerable<User> Get()
    {
        var entities = _repository.Get<Db.User>(
            orderBy: x => x.OrderBy(order => order.Id)
        );

        return _mapper.Map<IEnumerable<User>>(entities);
    }

    public User GetById(long id)
    {
        var entity = _repository.GetOne<Db.User>(
            x => x.Id == id
        );

        return _mapper.Map<User>(entity);
    }

    public User Create(User user)
    {
        if (string.IsNullOrWhiteSpace(user.FirstName)
            || string.IsNullOrWhiteSpace(user.LastName)
            || string.IsNullOrWhiteSpace(user.Email))
        {
            return null;
        }

        var entity = new Db.User
        {
            FirstName = user.FirstName,
            LastName = user.LastName,
            Email = user.Email,
            Position = user.Position,
            DateCreated = DateTime.UtcNow
        };

        _repository.Create(entity);
        _repository.Save();

        return _mapper.Map<User>(entity);
    }

    public User Update(User user)
    {
        if (string.IsNullOrWhiteSpace(user.FirstName)
            || string.IsNullOrWhiteSpace(user.LastName)
            || string.IsNullOrWhiteSpace(user.Email))
        {
            return null;
        }

        var entity = _repository.GetOne<Db.User>(
            x => x.Id == user.Id
        );
        if (entity == null)
        {
            return null;
        }

        entity.FirstName = user.FirstName;
        entity.LastName = user.LastName;
        entity.Email = user.Email;
        entity.Position = user.Position;

        _repository.Update(entity);
        _repository.Save();

        return _mapper.Map<User>(entity);
    }

    public bool Delete(long id)
    {
        var entity = _repository.GetOne<Db.User>(
            x => x.Id == id
        );
        if (entity == null)
        {
            return false;
        }

        _repository.Delete(entity);
        _repository.Save();

        return true;
    }

    public async System.Threading.Tasks.Task<User> CreateAsync(User user)
    {
        if (string.IsNullOrWhiteSpace(user.FirstName)
            || string.IsNullOrWhiteSpace(user.LastName)
            || string.IsNullOrWhiteSpace(user.Email))
        {
            return null;
        }

        var entity = new Db.User
        {
            FirstName = user.FirstName,
            LastName = user.LastName,
            Email = user.Email,
            Position = user.Position,
            DateCreated = DateTime.UtcNow
        };

        _repository.Create(entity);
        await _repository.SaveAsync();

        return _mapper.Map<User>(entity);
    }

    public async System.Threading.Tasks.Task<User> UpdateAsync(User user)
    {
        if (string.IsNullOrWhiteSpace(user.FirstName)
            || string.IsNullOrWhiteSpace(user.LastName)
            || string.IsNullOrWhiteSpace(user.Email))
        {
            return null;
        }

        var entity = _repository.GetOne<Db.User>(
            x => x.Id == user.Id
        );
        if (entity == null)
        {
            return null;
        }

        entity.FirstName = user.FirstName;
        entity.LastName = user.LastName;
        entity.Email = user.Email;
        entity.Position = user.Position;

        _repository.Update(entity);
        await _repository.SaveAsync();

        return _mapper.Map<User>(entity);
    }

    public async System.Threading.Tasks.Task<bool> DeleteAsync(long id)
    {
        var entity = _repository.GetOne<Db.User>(
            x => x.Id == id
        );
        if (entity == null)
        {
            return false;
        }

        _repository.Delete(entity);
        await _repository.SaveAsync();

        return true;
    }
}