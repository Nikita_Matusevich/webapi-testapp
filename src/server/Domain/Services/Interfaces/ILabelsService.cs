﻿using WebApiTestApp.Domain.Models;

namespace WebApiTestApp.Domain.Services.Interfaces;

public interface ILabelsService
{
    IEnumerable<Label> Get();

    Label GetById(long id);

    Label Create(Label label);

    Label Update(Label label);

    bool Delete(long id);

    System.Threading.Tasks.Task<Label> CreateAsync(Label label);

    System.Threading.Tasks.Task<Label> UpdateAsync(Label label);

    System.Threading.Tasks.Task<bool> DeleteAsync(long id);
}