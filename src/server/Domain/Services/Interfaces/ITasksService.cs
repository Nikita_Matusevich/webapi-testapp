﻿using WebApiTestApp.Domain.Models;
using Task = WebApiTestApp.Domain.Models.Task;
using TaskStatus = WebApiTestApp.Common.Enums.TaskStatus;

namespace WebApiTestApp.Domain.Services.Interfaces;

public interface ITasksService
{
    IEnumerable<Task> Get(GetTasksRequest request);

    Task GetById(long id);

    Task Create(Task task);

    Task Update(Task task);

    bool Delete(long id);

    bool AddLabel(long id, long labelId);

    bool DeleteLabel(long id, long labelId);

    bool AddUser(long id, long userId);

    bool DeleteUser(long id, long userId);

    System.Threading.Tasks.Task<Task> CreateAsync(Task task);

    System.Threading.Tasks.Task<Task> UpdateAsync(Task task);

    System.Threading.Tasks.Task<Task> UpdateTitleAsync(long id, string title);

    System.Threading.Tasks.Task<Task> UpdateDescriptionAsync(long id, string description);

    System.Threading.Tasks.Task<Task> UpdateStatusAsync(long id, TaskStatus status);

    System.Threading.Tasks.Task<Task> UpdateEstimatedTimeAsync(long id, DateTime estimatedTime);

    System.Threading.Tasks.Task<bool> DeleteAsync(long id);

    System.Threading.Tasks.Task<bool> AddLabelAsync(long id, long labelId);

    System.Threading.Tasks.Task<bool> DeleteLabelAsync(long id, long labelId);

    System.Threading.Tasks.Task<bool> AddUserAsync(long id, long userId);

    System.Threading.Tasks.Task<bool> DeleteUserAsync(long id, long userId);
}