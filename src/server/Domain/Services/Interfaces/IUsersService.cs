﻿using WebApiTestApp.Domain.Models;

namespace WebApiTestApp.Domain.Services.Interfaces;

public interface IUsersService
{
    IEnumerable<User> Get();

    User GetById(long id);

    User Create(User user);

    User Update(User user);

    bool Delete(long id);

    System.Threading.Tasks.Task<User> CreateAsync(User user);

    System.Threading.Tasks.Task<User> UpdateAsync(User user);

    System.Threading.Tasks.Task<bool> DeleteAsync(long id);
}