using System.Data.Common;
using System.Reflection;
using System.Text.Json;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.EntityFrameworkCore;
using WebApiTestApp.Common.ConfigurationOptions;
using WebApiTestApp.Common.Helpers;
using WebApiTestApp.DataAccess;
using WebApiTestApp.DataAccess.Repositories;
using WebApiTestApp.Domain.Services.Implementations;
using WebApiTestApp.Domain.Services.Interfaces;

var builder = WebApplication.CreateBuilder(args);

// Configuration options
var databaseConfigurationSection = builder.Configuration.GetSection(DatabaseOptions.Path);
builder.Services.Configure<DatabaseOptions>(databaseConfigurationSection);
var databaseConfigurationOptions = databaseConfigurationSection.Get<DatabaseOptions>();

var generalConfigurationSection = builder.Configuration.GetSection(GeneralOptions.Path);
builder.Services.Configure<DatabaseOptions>(generalConfigurationSection);
var generalConfigurationOptions = generalConfigurationSection.Get<GeneralOptions>();

if (databaseConfigurationOptions == null)
    throw new NullReferenceException(nameof(databaseConfigurationOptions));

// Add services to the container.

builder.Services
    .AddControllers()
    .AddJsonOptions(options =>
    {
        options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter(JsonNamingPolicy.CamelCase));
        options.JsonSerializerOptions.Converters.Add(new JsonTimeSpanConverter());
    });

var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
optionsBuilder
    .UseLazyLoadingProxies()
    .UseSqlServer(
        new DbConnectionStringBuilder
        {
            {"Server", databaseConfigurationOptions.SqlDataSource},
            {"Database", databaseConfigurationOptions.SqlDataCatalog},
            {"User Id", databaseConfigurationOptions.SqlDataUser},
            {"Password", databaseConfigurationOptions.SqlDataPassword},
            {"Application Name", Assembly.GetEntryAssembly()?.GetName().Name ?? "APPLICATION_NAME_NOT_FOUND"},
            {"Persist Security Info", "True"},
            {"MultipleActiveResultSets", "True"}
        }.ToString(),
        optionsAction =>
        {
            optionsAction.EnableRetryOnFailure();
        }
    );
builder.Services.AddScoped<AppDbContext>(instance => new AppDbContext(optionsBuilder.Options));
builder.Services.AddScoped<IRepository, AppDbContextFrameworkRepository>();

builder.Services.AddTransient<ILabelsService, LabelsService>();
builder.Services.AddTransient<ITasksService, TasksService>();
builder.Services.AddTransient<IUsersService, UsersService>();

builder.Services.AddAutoMapper(typeof(Program));

builder.Services.Configure<ForwardedHeadersOptions>(options =>
{
    options.ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
    options.KnownNetworks.Clear();
    options.KnownProxies.Clear();
});

const string defaultCorsPolicy = "__Default_Cors_Policy__";
builder.Services.AddCors(options =>
{
    options.AddPolicy(
        defaultCorsPolicy,
        policyBuilder =>
        {
            var allowedOrigin = builder.Configuration.GetSection("AllowedOrigins").Get<string[]>();
            if (builder.Environment.IsDevelopment())
            {
                policyBuilder
                    .AllowAnyOrigin()
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .SetIsOriginAllowed(origin => true);
            }
            else if (allowedOrigin != null && allowedOrigin.Length != 0)
            {
                policyBuilder
                    .WithOrigins(allowedOrigin)
                    .AllowAnyHeader()
                    .AllowAnyMethod();
            }
        }
    );
});

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

app.Use(async (context, next) =>
{
    context.Response.Headers["X-env"] = builder.Environment.EnvironmentName;
    context.Response.Headers["X-ver"] = generalConfigurationOptions?.GitHash ?? "LOCAL";
    context.Response.Headers["X-Content-Type-Options"] = "nosniff";
    context.Response.Headers["Strict-Transport-Security"] = "max-age=31536000; includeSubDomains";

    await next.Invoke();
});
app.UseForwardedHeaders();
app.UseDeveloperExceptionPage();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
else
{
    app.UseHttpsRedirection();
}

app.UseCors(defaultCorsPolicy);

app.UseAuthorization();

app.MapControllers();

app.Run();
